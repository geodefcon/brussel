from rest_framework import serializers
from brussel.api.models import Application

class ApplicationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Application
        fields = ('application', 'package_name', 'package_version_code')


