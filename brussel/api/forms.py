from django import forms
from brussel.api.models import Application


class ApplicationForm(forms.ModelForm):
    class Meta:
        model = Application
        fields = ['application_file']
