from django.views.generic import TemplateView
from django.shortcuts import render
from rest_framework import viewsets

from brussel.api.models import Application
from brussel.api.serializers import ApplicationSerializer
from brussel.api.forms import ApplicationForm


class ListApplications(viewsets.ModelViewSet):
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer


class UploadApplication(TemplateView):
    template_name = 'upload_application.html'

    def post(self, request, *args, **kwargs):
        context = {}

        form = ApplicationForm(request.POST, request.FILES)

        if form.is_valid():
            try:
                form.save()
                context['uploaded_file_url'] = True
            except Exception as e:
                context['error'] = 'Error in apk: ' + str(e)
        else:
            context['error'] = str(form.errors)

        return render(request, self.template_name, context=context)