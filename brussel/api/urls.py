from django.conf.urls import url
from django.urls import include
from rest_framework import routers

from brussel.api.views import UploadApplication, ListApplications


router = routers.DefaultRouter()
router.register(r'applications', ListApplications)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^upload', UploadApplication.as_view(), name="upload_app")
]
