import subprocess

from django.db import models
from django.conf import settings


class Application(models.Model):
    package_name = models.CharField(max_length=300)
    package_version_code = models.CharField(max_length=200)
    application = models.CharField(max_length=300, blank=True)
    application_file = models.FileField(upload_to='', blank=True)

    def save(self, *args, **kwargs):

        try:
            if self.application_file:
                path = settings.BASE_DIR + self.application_file.url
                super(Application, self).save(*args, **kwargs)

                data = get_package_info(path)

                self.package_name = data['name']
                self.package_version_code = data['versionCode']
                self.application = self.application_file.url

                super(Application, self).save(*args, **kwargs)

        except Exception as e:
            self.delete()
            raise Exception(e)



def get_package_info(apk_address):
    command = "aapt dump badging %s" % apk_address
    aapt_result = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True).communicate()[0]
    lines = aapt_result.decode()
    lines = lines.split("\n")

    data_dict = {}

    for line in lines:
        if line.startswith('package'):
            data = line.split(':')[1]
            data = data.split(' ')

            for elem in data:
                if not elem:
                    continue

                key_value = elem.split('=')

                ## remove ' and " symbols from key and value
                key = key_value[0].replace('"', '').replace("'", '')
                value = key_value[1].replace('"', '').replace("'", '')
                data_dict[key] = value

    return data_dict
